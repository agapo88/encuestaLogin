package curso.umg.gt.umgapplogin;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class StudentActivity extends AppCompatActivity {

    private RadioButton rBtnF, rBtnM, rBtnIS, rBtnIN;
    private CheckBox ckBoxBD, ckBoxFront, ckBoxBack;
    private EditText eTI, ed3, ed4, ed5, ed6, ed7, ed8, ed9, ed10;
    private ListView lv1;
    private List<String> listado;
    private ArrayAdapter<String> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student);

        rBtnF = (RadioButton) findViewById( R.id.rBtnF );
        rBtnM = (RadioButton) findViewById( R.id.rBtnM );
        rBtnIS = (RadioButton) findViewById( R.id.rBtnIS );
        rBtnIN = (RadioButton) findViewById( R.id.rBtnIN );
        ckBoxBack = (CheckBox) findViewById( R.id.ckBoxBack );
        ckBoxBD = (CheckBox) findViewById( R.id.ckBoxBD );
        ckBoxFront = (CheckBox) findViewById( R.id.ckBoxFront );

        eTI = (EditText) findViewById( R.id.eTI );  // ES IMPORTANTE POR QUE
        ed3 = (EditText) findViewById( R.id.ed3 );  // QUE ES SQL
        ed4 = (EditText) findViewById( R.id.ed4 );  // QUE ES PL/SQL
        ed5 = (EditText) findViewById( R.id.ed5 );  // QUE ES SQL Injection
        ed6 = (EditText) findViewById( R.id.ed6 );  // QUE ES SP
        ed7 = (EditText) findViewById( R.id.ed7 );  // QUE ES TRIGGER
        ed8 = (EditText) findViewById( R.id.ed8 );  // QUE ES FUNCION
        ed9 = (EditText) findViewById( R.id.ed9 );  // TIPOS DE CARDINALIDAD
        ed10 = (EditText) findViewById( R.id.ed10 );  // DIFERENCIA SQL/NOSQL

        listado = new ArrayList<>();
/*
        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, listado );
        lv1.setAdapter( adapter );
*/

    }

    public void guardarEncuesta( View view ){

        boolean error = false;
        boolean valorGeneroM = rBtnM.isChecked();
        boolean valorGeneroF = rBtnF.isChecked();
        boolean valorrBtnIS = rBtnIS.isChecked();
        boolean valorrBtnIN = rBtnIN.isChecked();
        String valorImportante = eTI.getText().toString();
        boolean valorFront = ckBoxFront.isChecked();
        boolean valorBack = ckBoxBack.isChecked();
        boolean valorBD = ckBoxBD.isChecked();
        String valorEd3 = ed3.getText().toString();
        String valorEd4 = ed4.getText().toString();
        String valorEd5 = ed5.getText().toString();
        String valorEd6 = ed6.getText().toString();
        String valorEd7 = ed7.getText().toString();
        String valorEd8 = ed8.getText().toString();
        String valorEd9 = ed9.getText().toString();
        String valorEd10 = ed10.getText().toString();

        // VALIDACIONES
        if( !valorGeneroF && !valorGeneroM ){
            error = true;
            Toast notification = Toast.makeText( this, "No ha seleccionado su genero", Toast.LENGTH_SHORT );
            notification.show();
        }
        else if( !valorrBtnIS &&  !valorrBtnIN ){
            error = true;
            Toast notification = Toast.makeText( this, "No ha seleccionado si es importante la carrera de Ingenieria", Toast.LENGTH_LONG );
            notification.show();
        }
        else if( valorImportante.length() <= 5 ){
            error = true;
            Toast notification = Toast.makeText( this, "Especifique por que cree que " + ( valorrBtnIS ? "SI" : "NO" ) + " es importante" , Toast.LENGTH_LONG );
            notification.show();
        }
        else if( !valorFront && !valorBack && !valorBD ){
            error = true;
            Toast notification = Toast.makeText( this, "Seleccione al menos una de sus especialidades" , Toast.LENGTH_LONG );
            notification.show();
        }
        else if( valorEd3.length() <= 5 ){
            error = true;
            Toast notification = Toast.makeText( this, "En la pregunta 3 su respuesta debe ser mayor a 5 caracteres" , Toast.LENGTH_LONG );
            notification.show();
        }
        else if( valorEd4.length() <= 5 ){
            error = true;
            Toast notification = Toast.makeText( this, "En la pregunta 4 su respuesta debe ser mayor a 5 caracteres" , Toast.LENGTH_LONG );
            notification.show();
        }
        else if( valorEd5.length() <= 5 ){
            error = true;
            Toast notification = Toast.makeText( this, "En la pregunta 5 su respuesta debe ser mayor a 5 caracteres" , Toast.LENGTH_LONG );
            notification.show();
        }
        else if( valorEd6.length() <= 5 ){
            error = true;
            Toast notification = Toast.makeText( this, "En la pregunta 6 su respuesta debe ser mayor a 5 caracteres" , Toast.LENGTH_LONG );
            notification.show();
        }
        else if( valorEd7.length() <= 5 ){
            error = true;
            Toast notification = Toast.makeText( this, "En la pregunta 7 su respuesta debe ser mayor a 5 caracteres" , Toast.LENGTH_LONG );
            notification.show();
        }
        else if( valorEd8.length() <= 5 ){
            error = true;
            Toast notification = Toast.makeText( this, "En la pregunta 8 su respuesta debe ser mayor a 5 caracteres" , Toast.LENGTH_LONG );
            notification.show();
        }
        else if( valorEd9.length() <= 5 ){
            error = true;
            Toast notification = Toast.makeText( this, "En la pregunta 9 su respuesta debe ser mayor a 5 caracteres" , Toast.LENGTH_LONG );
            notification.show();
        }
        else if( valorEd10.length() <= 5 ){
            error = true;
            Toast notification = Toast.makeText( this, "En la pregunta 10 su respuesta debe ser mayor a 5 caracteres" , Toast.LENGTH_LONG );
            notification.show();
        }


        if( !error ){
            Toast notification = Toast.makeText( this, "GUARDADO CORRECTAMENTE." , Toast.LENGTH_LONG );
            notification.show();
        }

    }

}
